import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Inicio5Component } from './inicio5.component';

describe('Inicio5Component', () => {
  let component: Inicio5Component;
  let fixture: ComponentFixture<Inicio5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Inicio5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Inicio5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
