import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { ContactanosComponent } from './components/contactanos/contactanos.component'
import { Inicio1Component } from './components/inicio1/inicio1.component';
import { Inicio2Component } from './components/inicio2/inicio2.component';
import { Inicio3Component } from './components/inicio3/inicio3.component';
import { Inicio4Component } from './components/inicio4/inicio4.component';
import { Inicio5Component } from './components/inicio5/inicio5.component';


const APP_ROUTES: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'noticias', component: NoticiasComponent },
  { path: 'contactanos', component: ContactanosComponent },
  { path: 'inicio1', component: Inicio1Component },
  { path: 'inicio2', component: Inicio2Component },
  { path: 'inicio3', component: Inicio3Component },
  { path: 'inicio4', component: Inicio4Component },
  { path: 'inicio5', component: Inicio5Component },
  { path: '**', pathMatch: 'full', redirectTo: '' }

];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);