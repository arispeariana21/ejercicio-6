import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { ContactanosComponent } from './components/contactanos/contactanos.component';
import { Inicio1Component } from './components/inicio1/inicio1.component';
import { Inicio2Component } from './components/inicio2/inicio2.component';
import { Inicio3Component } from './components/inicio3/inicio3.component';
import { Inicio4Component } from './components/inicio4/inicio4.component';
import { Inicio5Component } from './components/inicio5/inicio5.component';

import { APP_ROUTING } from './app.routes';
import { RouterModule } from '@angular/router';


  
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    InicioComponent,
    NoticiasComponent,
    ContactanosComponent,
    Inicio1Component,
    Inicio2Component,
    Inicio3Component,
    Inicio4Component,
    Inicio5Component
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
