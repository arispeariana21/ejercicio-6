import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Inicio4Component } from './inicio4.component';

describe('Inicio4Component', () => {
  let component: Inicio4Component;
  let fixture: ComponentFixture<Inicio4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Inicio4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Inicio4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
