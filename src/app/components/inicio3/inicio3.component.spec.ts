import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Inicio3Component } from './inicio3.component';

describe('Inicio3Component', () => {
  let component: Inicio3Component;
  let fixture: ComponentFixture<Inicio3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Inicio3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Inicio3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
